
=begin

Begin-Doc
Name: Local::UsageLogger
Type: module
Description: Stub set of routines wrapping around MST::UsageLogger
End-Doc

=cut

package Local::UsageLogger;
require Exporter;
use strict;

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);
use MST::UsageLogger;

@ISA    = qw(Exporter);
@EXPORT = ();

sub import {
    no strict 'refs';
    my $caller = caller;

    while ( my ( $name, $symbol ) = each %MST::UsageLogger:: ) {
        next if ( $name !~ /LogAPIUsage/o );

        my $imported = $caller . '::' . $name;
        *{$imported} = \*{$symbol};
    }
}

1;

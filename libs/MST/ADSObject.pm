
=begin

Begin-Doc
Name: MST::ADSObject
Type: module
Description: child class around Local::ADSObject to set defaults

End-Doc

=cut

package MST::ADSObject;
require 5.000;
use Exporter;
use strict;
use Local::ADSObject;
use MST::Env;
use MST::UsageLogger;
use Net::DNS;

use vars qw (@ISA @EXPORT);
@ISA    = qw(Local::ADSObject Exporter);
@EXPORT = qw();

# Map to super class
my $ErrorMsg;
*MST::ADSObject::ErrorMsg = *Local::ADSObject::ErrorMsg;

=begin
Begin-Doc
Name: new
Type: method
Description: creates object
Syntax: $obj->new(%params)
Comments: Same syntax/behavior as routine in ADSObject module.
End-Doc
=cut

sub new {
    my $self = shift;
    my @args = @_;
    my %opts = @_;

    my %base_opts = ( domain => "mst.edu", );

    return $self->SUPER::new( %base_opts, @_ );
}

# Begin-Doc
# Name: CreateSecurityGroup
# Type: method
# Description: Creates a security group netgroup
# Syntax: $crtusr = $ex->CreateSecurityGroup(group => $group)
# Returns: undef if success, else error
# End-Doc

sub CreateSecurityGroup {
    my $self = shift;
    my (%info) = @_;
    my ($group);
    my $ldap = $self->{ldap};
    $group = $info{group};
    my $dname = $info{displayname} || "S&T $group";

    return $self->SUPER::CreateSecurityGroup(
        group       => $group,
        displayName => $dname,
        ou          => "OU=Netgroups,OU=Services - Campus"
    );
}

# Begin-Doc
# Name: LookupDC
# Syntax: @hosts = &Local::ADSObject::LookupDC($domain)
# Syntax: @hosts = $self->LookupDC($domain)
# Description: Looks up domain controllers via SRV records in DNS for a domain, returns in preferred order
# End-Doc
sub LookupDC {
    my $domain = shift;
    if ( ref($domain) ) {
        $domain = shift;
    }

    # Hardwire for efficiency
    if ( $domain eq "mst.edu" ) {
        return ("mst-dc.mst.edu");
    }

    return &Local::ADSObject::LookupDC($domain);
}

# Begin-Doc
# Name: LookupGC
# Syntax: @hosts = &Local::ADSObject::LookupGC($domain)
# Syntax: @hosts = $self->LookupGC($domain)
# Description: Looks up global catalogs via SRV records in DNS for a domain, returns in preferred order
# Comments: NOTE - this is hardwired right now to look up the same as the DC... needs to be reworked to look up forest/etc.
# End-Doc
sub LookupGC {
    return &LookupDC(@_);
}

1;


=begin

Begin-Doc
Name: MST::PrivSys
Type: module
Description: Privilege system query/update
Comments:

The privilege system is designed to provide a centrally located system for
distributed administration of application access control, while still allowing
appropriate individuals to control access privileges for applications that they
are responsible for.  

There are two types of entries in the privilege system:

Application Group:

Allows grouping together a bunch of applications under a particular
heading. Users having an "application group" privilege means that they have
administrative rights for that group of applications, and can assign privileges
for applications in that group. They can also create new groups and
applications underneath the groups they have admin rights for.

Application:

This is an actual application. If a user has an "application" privilege they
can execute that application, or depending on how that application is using the
privileges, it may grant access to specific features of the application.

Application groups, and applications each have an associated privilege code.
Applications which are grouped under a particular application group will have a
privilege code beginning with the group code. For example, if the group code is
sysadm, and the application is set-quota, the application privilege code will
be sysadm:set-quota.

For the administrative routines, the routines are authorized if the owner of
the calling script has the privsys authorization to mae that change. i.e. The
script is running as 'fred'. If you were to authenticate to the web server as
'fred' and go to the privsys administration web apps, anything that you can do
there, the script should be able to do.

End-Doc

=cut

package MST::PrivSys;
require Exporter;
use strict;

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);

use Local::AuthSrv;
use Local::UsageLogger;
use MST::Env;
use Local::Encode;
use Local::SimpleRPC;
use Local::CurrentUser;

@ISA    = qw(Exporter);
@EXPORT = qw(
    PrivSys_CheckPriv
    PrivSys_CheckPrivRegex
    PrivSys_FetchPrivs
    PrivSys_FetchPrivsDetail

    PrivSys_CheckAdminPriv
    PrivSys_FetchAdminPrivs
    PrivSys_FetchAdminPrivsDetail

    PrivSys_FetchInfo
    PrivSys_FetchAllInfo
    PrivSys_RequirePriv
    PrivSys_RequirePrivRegex
    PrivSys_QuietRequirePriv
    PrivSys_QuietRequirePrivRegex

    PrivSys_FetchUsers
    PrivSys_FetchUsersNoNetGroups
    PrivSys_FetchUsersMulti
    PrivSys_FetchUsersNoNetGroupsMulti
    PrivSys_FetchNetGroups
    PrivSys_FetchNetGroupsMulti

    PrivSys_FetchAdminUsers
    PrivSys_FetchAdminUsersNoNetGroups
    PrivSys_FetchAdminUsersMulti
    PrivSys_FetchAdminUsersNoNetGroupsMulti
    PrivSys_FetchAdminNetGroups
    PrivSys_FetchAdminNetGroupsMulti

    PrivSys_ClearCache

    PrivSys_GrantPriv
    PrivSys_RevokePriv

    PrivSys_GrantAdminPriv
    PrivSys_RevokeAdminPriv

    PrivSys_CreatePriv
    PrivSys_DeletePriv
    PrivSys_UpdatePriv
);

# Begin-Doc
# Name: PRIVSYS_CACHE
# Type: variable
# Description: internal cache of lookup requests
# Comments: caches lookups so that database doesn't have to be queried every time
# cache is limited to per-session, and can be cleared with PrivSys_ClearCache.
# Access: internal
# End-Doc
my %PRIVSYS_CACHE       = ();
my %PRIVSYS_CACHE_TS    = ();
my %PRIVSYS_CP_CACHE    = ();
my %PRIVSYS_CP_CACHE_TS = ();
my $PRIVSYS_CACHE_LIFE  = 120;

my %PRIVSYS_ADMIN_CACHE       = ();
my %PRIVSYS_ADMIN_CACHE_TS    = ();
my %PRIVSYS_ADMIN_CP_CACHE    = ();
my %PRIVSYS_ADMIN_CP_CACHE_TS = ();
my $PRIVSYS_ADMIN_CACHE_LIFE  = 120;

my $PUBLIC_RPC = undef;
my $ADMIN_RPC  = undef;

BEGIN {
    &LogAPIUsage();

    # Force cache clear on module refresh
    %PRIVSYS_CACHE       = ();
    %PRIVSYS_CACHE_TS    = ();
    %PRIVSYS_CP_CACHE    = ();
    %PRIVSYS_CP_CACHE_TS = ();

    %PRIVSYS_ADMIN_CACHE       = ();
    %PRIVSYS_ADMIN_CACHE_TS    = ();
    %PRIVSYS_ADMIN_CP_CACHE    = ();
    %PRIVSYS_ADMIN_CP_CACHE_TS = ();
}

#
# Global Database handle
#
my $db;

# Begin-Doc
# Name: PrivSys_SimplePublicRPC
# Type: function
# Description: Helper routine to set up RPC connection for user apps
# Syntax: $rpc = &PrivSys_SimplePublicRPC()
# Access: internal
# End-Doc
sub PrivSys_SimplePublicRPC {

    if ($PUBLIC_RPC) {
        return $PUBLIC_RPC;
    }

    my $rpchost;
    my $env = &MST_Env();

    if ( $env eq "dev" ) {
        $rpchost = "itrpc-privsys-dev.mst.edu";
    }
    elsif ( $env eq "test" ) {
        $rpchost = "itrpc-privsys-test.mst.edu";
    }
    else {
        $rpchost = "itrpc-privsys.mst.edu";
    }

    $PUBLIC_RPC = new Local::SimpleRPC::Client(
        base_url => "http://${rpchost}/perl-bin",
        retries  => 2
    );

    return $PUBLIC_RPC;
}

# Begin-Doc
# Name: PrivSys_SimpleAdminRPC
# Type: function
# Description: Helper routine to set up RPC connection for user apps
# Syntax: $rpc = &PrivSys_SimpleAdminRPC()
# Access: internal
# End-Doc
sub PrivSys_SimpleAdminRPC {

    if ($ADMIN_RPC) {
        return $ADMIN_RPC;
    }

    my $rpchost;
    my $env = &MST_Env();

    if ( $env eq "dev" ) {
        $rpchost = "itrpc-privsys-dev.mst.edu";
    }
    elsif ( $env eq "test" ) {
        $rpchost = "itrpc-privsys-test.mst.edu";
    }
    else {
        $rpchost = "itrpc-privsys.mst.edu";
    }

    $ADMIN_RPC = new Local::SimpleRPC::Client(
        base_url => "https://${rpchost}/auth-perl-bin",
        retries  => 2
    );

    return $ADMIN_RPC;
}

# Begin-Doc
# Name: PrivSys_FetchUsers
# Type: function
# Description: Returns list of users with priv code
# Syntax: @users = &PrivSys_FetchUsers($code)
# Comments: This routine returns a list of all users that have the privilege
# code '$code'.
# Access: public
# End-Doc
sub PrivSys_FetchUsers {
    my ($code) = @_;
    if ( $code eq "" ) { return (); }

    &LogAPIUsage();

    my $rpc = &PrivSys_SimplePublicRPC();
    my $info = $rpc->FetchUsers( code => $code );

    if ( ref($info) eq "HASH" && ref( $info->{$code} ) eq "ARRAY" ) {
        return @{ $info->{$code} };
    }
    else {
        return ();
    }
}

# Begin-Doc
# Name: PrivSys_FetchAdminUsers
# Type: function
# Description: Returns list of users with admin access to priv code
# Syntax: @users = &PrivSys_FetchAdminUsers($code)
# Comments: This routine returns a list of all users that have the privilege
# code '$code'.
# Access: public
# End-Doc
sub PrivSys_FetchAdminUsers {
    my ($code) = @_;
    if ( $code eq "" ) { return (); }

    &LogAPIUsage();

    my $rpc = &PrivSys_SimplePublicRPC();
    my $info = $rpc->FetchAdminUsers( code => $code );

    if ( ref($info) eq "HASH" && ref( $info->{$code} ) eq "ARRAY" ) {
        return @{ $info->{$code} };
    }
    else {
        return ();
    }
}

# Begin-Doc
# Name: PrivSys_FetchUsersNoNetGroups
# Type: function
# Description: Returns list of users with priv code that have been granted directly
# Syntax: @users = &PrivSys_FetchUsersNoNetGroups($code)
# Comments: This routine returns a list of all users that have the privilege
# code '$code'.
# Access: public
# End-Doc
sub PrivSys_FetchUsersNoNetGroups {
    my ($code) = @_;
    if ( $code eq "" ) { return (); }

    &LogAPIUsage();

    my $rpc = &PrivSys_SimplePublicRPC();
    my $info = $rpc->FetchUsersNoNetGroups( code => $code );

    if ( ref($info) eq "HASH" && ref( $info->{$code} ) eq "ARRAY" ) {
        return @{ $info->{$code} };
    }
    else {
        return ();
    }
}

# Begin-Doc
# Name: PrivSys_FetchAdminUsersNoNetGroups
# Type: function
# Description: Returns list of users with admin access to priv code that have been granted directly
# Syntax: @users = &PrivSys_FetchAdminUsersNoNetGroups($code)
# Comments: This routine returns a list of all users that have the privilege
# code '$code'.
# Access: public
# End-Doc
sub PrivSys_FetchAdminUsersNoNetGroups {
    my ($code) = @_;
    if ( $code eq "" ) { return (); }

    &LogAPIUsage();

    my $rpc = &PrivSys_SimplePublicRPC();
    my $info = $rpc->FetchAdminUsersNoNetGroups( code => $code );

    if ( ref($info) eq "HASH" && ref( $info->{$code} ) eq "ARRAY" ) {
        return @{ $info->{$code} };
    }
    else {
        return ();
    }
}

# Begin-Doc
# Name: PrivSys_FetchNetGroups
# Type: function
# Description: Returns list of netgroups with priv code
# Syntax: @users = &PrivSys_FetchNetGroups($code)
# Comments: This routine returns a list of all netgroups that have the privilege
# code '$code'.
# Access: public
# End-Doc
sub PrivSys_FetchNetGroups {
    my ($code) = @_;
    if ( $code eq "" ) { return (); }

    &LogAPIUsage();

    my $rpc = &PrivSys_SimplePublicRPC();
    my $info = $rpc->FetchNetGroups( code => $code );

    if ( ref($info) eq "HASH" && ref( $info->{$code} ) eq "ARRAY" ) {
        return @{ $info->{$code} };
    }
    else {
        return ();
    }
}

# Begin-Doc
# Name: PrivSys_FetchAdminNetGroups
# Type: function
# Description: Returns list of netgroups with admin access to priv code
# Syntax: @users = &PrivSys_FetchAdminNetGroups($code)
# Comments: This routine returns a list of all netgroups that have the privilege
# code '$code'.
# Access: public
# End-Doc
sub PrivSys_FetchAdminNetGroups {
    my ($code) = @_;
    if ( $code eq "" ) { return (); }

    &LogAPIUsage();

    my $rpc = &PrivSys_SimplePublicRPC();
    my $info = $rpc->FetchAdminNetGroups( code => $code );

    if ( ref($info) eq "HASH" && ref( $info->{$code} ) eq "ARRAY" ) {
        return @{ $info->{$code} };
    }
    else {
        return ();
    }
}

# Begin-Doc
# Name: PrivSys_FetchUsersMulti
# Type: function
# Description: Returns list of users with each priv code
# Syntax: %users_by_code = &PrivSys_FetchUsersMulti(@codes)
# Comments: Returns hash keyed on code, values are arrays of users with that code.
# Access: public
# End-Doc
sub PrivSys_FetchUsersMulti {
    my @codes = @_;

    &LogAPIUsage();

    my $rpc = &PrivSys_SimplePublicRPC();

    my @req = ();
    foreach my $code (@codes) {
        push( @req, "code", $code );
    }
    my $info = $rpc->FetchUsers(@req);
    return %{$info};
}

# Begin-Doc
# Name: PrivSys_FetchAdminUsersMulti
# Type: function
# Description: Returns list of users with admin access to each priv code
# Syntax: %users_by_code = &PrivSys_FetchAdminUsersMulti(@codes)
# Comments: Returns hash keyed on code, values are arrays of users with that code.
# Access: public
# End-Doc
sub PrivSys_FetchAdminUsersMulti {
    my @codes = @_;

    &LogAPIUsage();

    my $rpc = &PrivSys_SimplePublicRPC();

    my @req = ();
    foreach my $code (@codes) {
        push( @req, "code", $code );
    }
    my $info = $rpc->FetchAdminUsers(@req);
    return %{$info};
}

# Begin-Doc
# Name: PrivSys_FetchUsersNoNetGroupsMulti
# Type: function
# Description: Returns list of users with each priv code, only directly granted
# Syntax: %users_by_code = &PrivSys_FetchUsersNoNetGroupsMulti(@codes)
# Comments: Returns hash keyed on code, values are arrays of users with that code.
# Access: public
# End-Doc
sub PrivSys_FetchUsersNoNetGroupsMulti {
    my @codes = @_;

    &LogAPIUsage();

    my $rpc = &PrivSys_SimplePublicRPC();

    my @req = ();
    foreach my $code (@codes) {
        push( @req, "code", $code );
    }
    my $info = $rpc->FetchUsersNoNetGroups(@req);
    return %{$info};
}

# Begin-Doc
# Name: PrivSys_FetchAdminUsersNoNetGroupsMulti
# Type: function
# Description: Returns list of users with admin access to each priv code, only directly granted
# Syntax: %users_by_code = &PrivSys_FetchAdminUsersNoNetGroupsMulti(@codes)
# Comments: Returns hash keyed on code, values are arrays of users with that code.
# Access: public
# End-Doc
sub PrivSys_FetchAdminUsersNoNetGroupsMulti {
    my @codes = @_;

    &LogAPIUsage();

    my $rpc = &PrivSys_SimplePublicRPC();

    my @req = ();
    foreach my $code (@codes) {
        push( @req, "code", $code );
    }
    my $info = $rpc->FetchAdminUsersNoNetGroups(@req);
    return %{$info};
}

# Begin-Doc
# Name: PrivSys_FetchNetGroupsMulti
# Type: function
# Description: Returns list of netgroups with each priv code
# Syntax: %groups_by_code = &PrivSys_FetchNetGroupsMulti(@codes)
# Comments: Returns hash keyed on code, values are arrays of netgroups with that code.
# Access: public
# End-Doc
sub PrivSys_FetchNetGroupsMulti {
    my @codes = @_;

    &LogAPIUsage();

    my $rpc = &PrivSys_SimplePublicRPC();

    my @req = ();
    foreach my $code (@codes) {
        push( @req, "code", $code );
    }
    my $info = $rpc->FetchNetGroups(@req);
    return %{$info};
}

# Begin-Doc
# Name: PrivSys_FetchAdminNetGroupsMulti
# Type: function
# Description: Returns list of netgroups with admin access to each priv code
# Syntax: %groups_by_code = &PrivSys_FetchAdminNetGroupsMulti(@codes)
# Comments: Returns hash keyed on code, values are arrays of netgroups with that code.
# Access: public
# End-Doc
sub PrivSys_FetchAdminNetGroupsMulti {
    my @codes = @_;

    &LogAPIUsage();

    my $rpc = &PrivSys_SimplePublicRPC();

    my @req = ();
    foreach my $code (@codes) {
        push( @req, "code", $code );
    }
    my $info = $rpc->FetchAdminNetGroups(@req);
    return %{$info};
}

# Begin-Doc
# Name: PrivSys_FetchInfo
# Type: function
# Description: Fetches information on a privilege code
# Syntax: %info = &PrivSys_FetchInfo($code)
# Comments: This routine will load an associative array with information
# 	about the code that is being queried. Keys are 'type', 'code', 'name',
# 	and 'url'.
# Access: public
# End-Doc
sub PrivSys_FetchInfo {
    my ($code) = @_;
    if ( $code eq "" ) { return (); }

    &LogAPIUsage();

    my $rpc = &PrivSys_SimplePublicRPC();
    my $info = $rpc->FetchInfo( code => $code );
    return %$info;
}

# Begin-Doc
# Name: PrivSys_FetchAllInfo
# Type: function
# Description: Fetches info on all privilege codes
# Syntax: $info = &PrivSys_FetchAllInfo([$prefix])
# Comments: This routine will return a reference to a hash, keyed on privcode
# containing references to hashes equivalent to those returned by a
# PrivSys_FetchInfo call for each privcode. If prefix is specified, will only return codes with that prefix.
# Access: public
# End-Doc
sub PrivSys_FetchAllInfo {
    my $prefix = shift;

    &LogAPIUsage();

    my $rpc = &PrivSys_SimplePublicRPC();
    my $res = $rpc->FetchAllInfo( prefix => $prefix );

    return $res;
}

# Begin-Doc
# Name: PrivSys_CheckPriv
# Type: function
# Description: Retrieves whether a user has a priv or not
# Syntax: $truefalse = &PrivSys_CheckPriv($userid, $code)
# Comments: This routine will return whether a user has a given priv or not. Note, if this
#  routine will be called multiple times for a given user with different codes in the
#  same app, it would be more efficient to call FetchPrivs instead.
# Example:
#        if ( &PrivSys_CheckPriv($userid, "my:app:code") )
#        {
#                # user has privilege
#        }
#        else
#        {
#                # user doesn't have privilege
#        }
# Access: public
# End-Doc
sub PrivSys_CheckPriv {
    my ( $user, $code ) = @_;
    my $key = join( "\0", $user, $code );
    my $tmp;

    &LogAPIUsage();

    # Could probably make this more intelligent, but for now, this works good I think
    $tmp = $PRIVSYS_CP_CACHE{$key};
    if ( defined($tmp)
        && ( time - $PRIVSYS_CP_CACHE_TS{$key} < $PRIVSYS_CACHE_LIFE ) )
    {
        return $PRIVSYS_CP_CACHE{$key};
    }

    # If we've called FetchPrivs, be smart about using it's cache instead of the RPC call.
    # If either public or user has the perms cached and granted, cache the positive
    my $tmp1 = $PRIVSYS_CACHE{$user};
    my $tmp2 = $PRIVSYS_CACHE{"public"};
    if ( ( $tmp1 && $tmp1->{$code} ) || ( $tmp2 && $tmp2->{$code} ) ) {
        $PRIVSYS_CP_CACHE{$key}    = 1;
        $PRIVSYS_CP_CACHE_TS{$key} = time;
        return $PRIVSYS_CP_CACHE{$key};
    }

    # If cache was found for both public and user, and not granted, we can cache the negative
    if ( $tmp1 && $tmp2 ) {
        $PRIVSYS_CP_CACHE{$key}    = 0;
        $PRIVSYS_CP_CACHE_TS{$key} = time;
        return $PRIVSYS_CP_CACHE{$key};
    }

    my $rpc = &PrivSys_SimplePublicRPC();
    my ($res) = $rpc->CheckPriv( user => $user, code => $code );

    $PRIVSYS_CP_CACHE{$key}    = $res;
    $PRIVSYS_CP_CACHE_TS{$key} = time;
    return $res;
}

# Begin-Doc
# Name: PrivSys_CheckPrivRegex
# Type: function
# Description: Retrieves whether a user has a priv or not
# Syntax: $truefalse = &PrivSys_CheckPrivRegex($userid, $regex)
# Comments: This routine will return whether a user has privilege matching the regex or not. Note, if this
#  routine will be called multiple times for a given user with different codes in the
#  same app, it would be more efficient to call FetchPrivs instead.
# Example:
#        if ( &PrivSys_CheckPrivRegex($userid, '^my:app:code:.*') )
#        {
#                # user has a privilege starting with my:app:code:
#        }
#        else
#        {
#                # user doesn't have privilege
#        }
# Access: public
# End-Doc
sub PrivSys_CheckPrivRegex {
    my ( $user, $regex ) = @_;

    &LogAPIUsage();

    my %privs = ( &PrivSys_FetchPrivs($user), &PrivSys_FetchPrivs("public") );
    my $re = qr/$regex/;
    if ($re) {
        foreach my $priv ( sort( keys(%privs) ) ) {
            if ( $priv =~ /$re/ ) {
                return 1;
            }
        }
    }
    return 0;
}

# Begin-Doc
# Name: PrivSys_CheckAdminPriv
# Type: function
# Description: Retrieves whether a user has a priv or not
# Syntax: $truefalse = &PrivSys_CheckAdminPriv($userid, $code)
# Comments: This routine will return whether a user has a given priv or not. Note, if this
#  routine will be called multiple times for a given user with different codes in the
#  same app, it would be more efficient to call FetchPrivs instead.
# Example:
#        if ( &PrivSys_CheckAdminPriv($userid, "my:app:code") )
#        {
#                # user has privilege
#        }
#        else
#        {
#                # user doesn't have privilege
#        }
# Access: public
# End-Doc
sub PrivSys_CheckAdminPriv {
    my ( $user, $code ) = @_;
    my $key = join( "\0", $user, $code );
    my $tmp;

    &LogAPIUsage();

    # Could probably make this more intelligent, but for now, this works good I think
    $tmp = $PRIVSYS_ADMIN_CP_CACHE{$key};
    if ( defined($tmp)
        && ( time - $PRIVSYS_ADMIN_CP_CACHE_TS{$key} < $PRIVSYS_ADMIN_CACHE_LIFE ) )
    {
        return $PRIVSYS_ADMIN_CP_CACHE{$key};
    }

    # If we've called FetchPrivs, be smart about using it's cache instead of the RPC call.
    # If either public or user has the perms cached and granted, cache the positive
    my $tmp1 = $PRIVSYS_ADMIN_CACHE{$user};
    my $tmp2 = $PRIVSYS_ADMIN_CACHE{"public"};
    if ( ( $tmp1 && $tmp1->{$code} ) || ( $tmp2 && $tmp2->{$code} ) ) {
        $PRIVSYS_ADMIN_CP_CACHE{$key}    = 1;
        $PRIVSYS_ADMIN_CP_CACHE_TS{$key} = time;
        return $PRIVSYS_ADMIN_CP_CACHE{$key};
    }

    # If cache was found for both public and user, and not granted, we can cache the negative
    if ( $tmp1 && $tmp2 ) {
        $PRIVSYS_ADMIN_CP_CACHE{$key}    = 0;
        $PRIVSYS_ADMIN_CP_CACHE_TS{$key} = time;
        return $PRIVSYS_ADMIN_CP_CACHE{$key};
    }

    my $rpc = &PrivSys_SimplePublicRPC();
    my ($res) = $rpc->CheckAdminPriv( user => $user, code => $code );

    $PRIVSYS_ADMIN_CP_CACHE{$key}    = $res;
    $PRIVSYS_ADMIN_CP_CACHE_TS{$key} = time;
    return $res;
}

# Begin-Doc
# Name: PrivSys_FetchPrivs
# Type: function
# Description: Fetches list of priv codes granted to a user
# Syntax: %hash = &PrivSys_FetchPrivs($userid)
# Comments: This routine will load an associative array with the keys
# being he privileges that userid has. If the userid has the
# privilege, the value will be 1, if they do not, the value will
# be 0 or undefined.
# Example:
#        %privs = &PrivSys_FetchPrivs($userid);
#        if ( $privs{"the-privilege-code-here"} )
#        {
#                # user has privilege
#        }
#        else
#        {
#                # user doesn't have privilege
#        }
# Access: public
# End-Doc
sub PrivSys_FetchPrivs {
    my @users = @_;
    my ( $qry, $cid, $code, %privs, $tmp );
    my $key = join( "\0", @users );

    &LogAPIUsage();

    # Could probably make this more intelligent, but for now, this works good I think
    $tmp = $PRIVSYS_CACHE{$key};
    if ( $tmp && ( time - $PRIVSYS_CACHE_TS{$key} < $PRIVSYS_CACHE_LIFE ) ) {
        return %{ $PRIVSYS_CACHE{$key} };
    }

    my $rpc = &PrivSys_SimplePublicRPC();
    my @req = ();
    foreach my $user (@users) {
        push( @req, "user", $user );
    }
    my @privs = $rpc->FetchPrivs(@req);

    my $result = { map { $_ => 1 } @privs };
    $PRIVSYS_CACHE{$key}    = $result;
    $PRIVSYS_CACHE_TS{$key} = time;

    return %{$result};
}

# Begin-Doc
# Name: PrivSys_FetchAdminPrivs
# Type: function
# Description: Fetches list of priv codes with admin access granted to a user
# Syntax: %hash = &PrivSys_FetchAdminPrivs(@users)
# Comments: This routine will load an associative array with the keys
# being he privileges that list of userids has. If any of the userids has the
# privilege, the value will be 1, if they do not, the value will
# be 0 or undefined. Admin privs are by design inherited, so if you have A:B, you'll
# also have A:B:C.
# Example:
#        %privs = &PrivSys_FetchAdminPrivs($userid);
#        if ( $privs{"the-privilege-code-here"} )
#        {
#                # user has privilege
#        }
#        else
#        {
#                # user doesn't have privilege
#        }
# Access: public
# End-Doc
sub PrivSys_FetchAdminPrivs {
    my @users = @_;
    my ( $qry, $cid, $code, %privs, $tmp );
    my $key = join( "\0", @users );

    &LogAPIUsage();

    # Could probably make this more intelligent, but for now, this works good I think
    $tmp = $PRIVSYS_ADMIN_CACHE{$key};
    if ( $tmp
        && ( time - $PRIVSYS_ADMIN_CACHE_TS{$key} < $PRIVSYS_ADMIN_CACHE_LIFE ) )
    {
        return %{ $PRIVSYS_ADMIN_CACHE{$key} };
    }

    my $rpc = &PrivSys_SimplePublicRPC();
    my @req = ();
    foreach my $user (@users) {
        push( @req, "user", $user );
    }
    my @privs = $rpc->FetchAdminPrivs(@req);

    my $result = { map { $_ => 1 } @privs };
    $PRIVSYS_ADMIN_CACHE{$key}    = $result;
    $PRIVSYS_ADMIN_CACHE_TS{$key} = time;

    return %{$result};
}

# Begin-Doc
# Name: PrivSys_FetchPrivsDetail
# Type: function
# Description: Combination of FetchPrivs and FetchInfo
# Syntax: %assoc = &PrivSys_FetchPrivsDetail($userid)
# Comments: This routine combines FetchPrivs and FetchInfo. It returns
#	a assoc array of which the keys are the privilege codes and the values
#	are references to the same hash that would be returned by FetchInfo.
# Access: public
# End-Doc
sub PrivSys_FetchPrivsDetail {
    my ($user) = @_;
    my ( $qry, $cid, $code, %privs, $tmp );
    if ( $user eq "" ) { return (); }

    &LogAPIUsage();

    my $rpc = &PrivSys_SimplePublicRPC();
    my $res = $rpc->FetchPrivsDetail( user => $user );

    return %$res;
}

# Begin-Doc
# Name: PrivSys_FetchAdminPrivsDetail
# Type: function
# Description: Combination of FetchAdminPrivs and FetchInfo
# Syntax: %assoc = &PrivSys_FetchAdminPrivsDetail($userid)
# Comments: This routine combines FetchAdminPrivs and FetchInfo. It returns
#	a assoc array of which the keys are the privilege codes and the values
#	are references to the same hash that would be returned by FetchInfo.
# Access: public
# End-Doc
sub PrivSys_FetchAdminPrivsDetail {
    my ($user) = @_;
    my ( $qry, $cid, $code, %privs, $tmp );
    if ( $user eq "" ) { return (); }

    &LogAPIUsage();

    my $rpc = &PrivSys_SimplePublicRPC();
    my $res = $rpc->FetchAdminPrivsDetail( user => $user );

    return %$res;
}

# Begin-Doc
# Name: PrivSys_RequirePriv
# Type: function
# Description: Requires that authenticated user has a particular privcode
# Syntax: &PrivSys_RequirePriv($privilege)
# Comments: This routine will check to see if the user who authenticated to
# access the current CGI script (stored in environment variable REMOTE_USER)
# has the required privilege. Privilege is a string containing the
# application or group privilege code you wish to require. If the userid has
# the particular privilege this routine will not do anything, however, if
# the userid does not have the privilege, this routine will print out a
# Content-Type header, followed by an error message indicating that the
# userid does not have access to the application. If the userid does not
# have the privilege, this routine will terminate the script after printing
# the error message.
# Access: public
# End-Doc
sub PrivSys_RequirePriv {
    my ($priv)  = @_;
    my (%privs) = ();

    &LogAPIUsage();

    if ( !&PrivSys_CheckPriv( $ENV{REMOTE_USER}, $priv ) ) {
        print "Content-Type: text/html\n\n";

        print "<H3>Permission Denied!</H3>\n";
        print "The id you authenticated as (";
        print $ENV{"REMOTE_USER"}, ") does not have ";
        print "the privilege required to access this application.";
        print "<P>\n";
        exit;
    }

}

# Begin-Doc
# Name: PrivSys_RequirePrivRegex
# Type: function
# Description: Requires that authenticated user has at least one priv code matching specified regex
# Syntax: &PrivSys_RequirePrivRegex($regex)
# Comments: This routine will check to see if the user who authenticated to
# access the current CGI script (stored in environment variable REMOTE_USER)
# has the required privilege. $regex is a string containing a regular expression
# to match against the codes that the user has. Be sure to bind with ^ and $ as
# applicable. If the userid does not have the privilege, this routine will print out a
# Content-Type header, followed by an error message indicating that the
# userid does not have access to the application. If the userid does not
# have the privilege, this routine will terminate the script after printing
# the error message.
# Access: public
# End-Doc
sub PrivSys_RequirePrivRegex {
    my ($regex) = @_;

    &LogAPIUsage();

    if ( !&PrivSys_CheckPrivRegex( $ENV{REMOTE_USER}, $regex ) ) {
        print "Content-Type: text/html\n\n";

        print "<H3>Permission Denied!</H3>\n";
        print "The id you authenticated as (";
        print $ENV{"REMOTE_USER"}, ") does not have ";
        print "the privilege required to access this application.";
        print "<P>\n";
        exit;
    }
}

# Begin-Doc
# Name: PrivSys_QuietRequirePriv
# Type: function
# Description: Requires that authenticated user has a particular privcode
# Syntax: &PrivSys_QuietRequirePriv($privilege)
# Comments: This routine is similar to PrivSys_RequirePriv, except that it will not output
# anything if the user doesn't have the priv, it will just immediately die. This is so that it
# can conveniently be used in an eval or for the RPC mechanism.
# Access: public
# End-Doc
sub PrivSys_QuietRequirePriv {
    my ($priv)  = @_;
    my (%privs) = ();

    &LogAPIUsage();

    if ( !&PrivSys_CheckPriv( $ENV{REMOTE_USER}, $priv ) ) {
        die "Permission Denied, Required Priv ($priv) not granted.";
    }
}

# Begin-Doc
# Name: PrivSys_QuietRequirePrivRegex
# Type: function
# Description: Requires that authenticated user has at least one priv code matching specified regex
# Syntax: &PrivSys_QuietRequirePrivRegex($regex)
# Comments: This routine is similar to PrivSys_RequirePrivRegex, except that it will not output
# anything if the user doesn't have the priv, it will just immediately die. This is so that it
# can conveniently be used in an eval or for the RPC mechanism.
# Access: public
# End-Doc
sub PrivSys_QuietRequirePrivRegex {
    my ($regex) = @_;

    &LogAPIUsage();

    if ( !&PrivSys_CheckPrivRegex( $ENV{REMOTE_USER}, $regex ) ) {
        die "Permission Denied, Required Priv ($regex) not granted.";
    }
}

# Begin-Doc
# Name: PrivSys_ClearCache
# Type: function
# Description: Clears privsys query cache
# Syntax: &PrivSys_ClearCache()
# Comments: Normally this should never be used as it will only slow down.
# The appropriate time for this to be used is in an application that is
# making privsys changes that itself uses privsys to control display, and
# where the display might need changed within a single execution. (i.e. if
# you grant yourself admin rights using the privilege editor - without the
# ability to clear the cache, you would have to reload twice, since the
# first time through, it won't have made the change yet.) If this doesn't
# make any sense to you, you don't need to use this routine.
# Access: public
# End-Doc
sub PrivSys_ClearCache {
    my (@tmp) = @_;
    my ($user);

    &LogAPIUsage();

    if ( $#tmp >= 0 ) {
        foreach $user (@tmp) {
            delete $PRIVSYS_CACHE{$user};
            delete $PRIVSYS_CACHE_TS{$user};
            delete $PRIVSYS_CP_CACHE{$user};
            delete $PRIVSYS_CP_CACHE_TS{$user};
            delete $PRIVSYS_ADMIN_CACHE{$user};
            delete $PRIVSYS_ADMIN_CACHE_TS{$user};
            delete $PRIVSYS_ADMIN_CP_CACHE{$user};
            delete $PRIVSYS_ADMIN_CP_CACHE_TS{$user};
        }
    }
    else {
        %PRIVSYS_CACHE             = ();
        %PRIVSYS_CACHE_TS          = ();
        %PRIVSYS_CP_CACHE          = ();
        %PRIVSYS_CP_CACHE_TS       = ();
        %PRIVSYS_ADMIN_CACHE       = ();
        %PRIVSYS_ADMIN_CACHE_TS    = ();
        %PRIVSYS_ADMIN_CP_CACHE    = ();
        %PRIVSYS_ADMIN_CP_CACHE_TS = ();
    }
}

# Begin-Doc
# Name: PrivSys_GrantPriv
# Type: function
# Description: Grants a privilege to a user
# Syntax: &PrivSys_GrantPriv($userid,$privcode)
# Comments: If the calling routine has the appropriate permissions,
#	this will grant the given privilege to $userid.
# Access: public
# End-Doc
sub PrivSys_GrantPriv {
    my ( $userid, $code ) = @_;
    &LogAPIUsage();

    my $curuser = &Local_CurrentUser();
    my $authuser;
    if ( $curuser eq "privsys" ) {
        $authuser = $ENV{REMOTE_USER};
    }

    my $rpc = &PrivSys_SimpleAdminRPC();
    my $res = $rpc->GrantPriv(
        authuser => $authuser,
        user     => $userid,
        code     => $code
    );

    return ();
}

# Begin-Doc
# Name: PrivSys_GrantAdminPriv
# Type: function
# Description: Grants a privilege to a user
# Syntax: &PrivSys_GrantAdminPriv($userid,$privcode)
# Comments: If the calling routine has the appropriate permissions,
#	this will grant the given privilege to $userid.
# Access: public
# End-Doc
sub PrivSys_GrantAdminPriv {
    my ( $userid, $code ) = @_;
    &LogAPIUsage();

    my $curuser = &Local_CurrentUser();
    my $authuser;
    if ( $curuser eq "privsys" ) {
        $authuser = $ENV{REMOTE_USER};
    }

    my $rpc = &PrivSys_SimpleAdminRPC();
    my $res = $rpc->GrantAdminPriv(
        authuser => $authuser,
        user     => $userid,
        code     => $code
    );

    return ();
}

# Begin-Doc
# Name: PrivSys_CreatePriv
# Type: function
# Description: Creates a privilege code
# Syntax: &PrivSys_CreatePriv($type,$code,$url,$name)
# Comments:
# Access: public
# End-Doc
sub PrivSys_CreatePriv {
    my ( $type, $code, $url, $name ) = @_;
    &LogAPIUsage();

    my $curuser = &Local_CurrentUser();
    my $authuser;
    if ( $curuser eq "privsys" ) {
        $authuser = $ENV{REMOTE_USER};
    }

    my $rpc = &PrivSys_SimpleAdminRPC();
    my $res = $rpc->CreatePriv(
        authuser => $authuser,
        type     => $type,
        code     => $code,
        url      => $url,
        name     => $name
    );

    return ();
}

# Begin-Doc
# Name: PrivSys_UpdatePriv
# Type: function
# Description: Updates a privilege code
# Syntax: &PrivSys_UpdatePriv($type,$code,$url,$name)
# Comments: This syntax is broken, $type can't be changed, need to fix later, but impacts apps
# Access: public
# End-Doc
sub PrivSys_UpdatePriv {
    my ( $type, $code, $url, $name ) = @_;
    &LogAPIUsage();

    my $curuser = &Local_CurrentUser();
    my $authuser;
    if ( $curuser eq "privsys" ) {
        $authuser = $ENV{REMOTE_USER};
    }

    my $rpc = &PrivSys_SimpleAdminRPC();
    my $res = $rpc->UpdatePriv(
        authuser => $authuser,
        code     => $code,
        url      => $url,
        name     => $name
    );

    return ();
}

# Begin-Doc
# Name: PrivSys_DeletePriv
# Type: function
# Description: Deletes a privilege code
# Syntax: &PrivSys_DeletePriv($code)
# Comments:
# Access: public
# End-Doc
sub PrivSys_DeletePriv {
    my ($code) = @_;
    &LogAPIUsage();

    my $curuser = &Local_CurrentUser();
    my $authuser;
    if ( $curuser eq "privsys" ) {
        $authuser = $ENV{REMOTE_USER};
    }

    my $rpc = &PrivSys_SimpleAdminRPC();
    my $res = $rpc->DeletePriv( authuser => $authuser, code => $code );

    return ();
}

# Begin-Doc
# Name: PrivSys_RevokePriv
# Type: function
# Description: Revokes a permission from a user
# Syntax: &PrivSys_RevokePriv($userid,$privcode);
# Comments: If the calling routine has the appropriate permissions,
#	this will revoke the given privilege from $userid.
# Access: public
# End-Doc
sub PrivSys_RevokePriv {
    my ( $userid, $code ) = @_;
    &LogAPIUsage();

    my $curuser = &Local_CurrentUser();
    my $authuser;
    if ( $curuser eq "privsys" ) {
        $authuser = $ENV{REMOTE_USER};
    }

    my $rpc = &PrivSys_SimpleAdminRPC();
    my $res = $rpc->RevokePriv(
        authuser => $authuser,
        user     => $userid,
        code     => $code,
    );

    return ();
}

# Begin-Doc
# Name: PrivSys_RevokeAdminPriv
# Type: function
# Description: Revokes a permission from a user
# Syntax: &PrivSys_RevokeAdminPriv($userid,$privcode);
# Comments: If the calling routine has the appropriate permissions,
#	this will revoke the given privilege from $userid.
# Access: public
# End-Doc
sub PrivSys_RevokeAdminPriv {
    my ( $userid, $code ) = @_;
    &LogAPIUsage();

    my $curuser = &Local_CurrentUser();
    my $authuser;
    if ( $curuser eq "privsys" ) {
        $authuser = $ENV{REMOTE_USER};
    }

    my $rpc = &PrivSys_SimpleAdminRPC();
    my $res = $rpc->RevokeAdminPriv(
        authuser => $authuser,
        user     => $userid,
        code     => $code,
    );

    return ();
}

1;


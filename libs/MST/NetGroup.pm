
=begin
Begin-Doc
Name: MST::NetGroup
Type: module
Description: NIS user netgroup maintenance
End-Doc
=cut

package MST::NetGroup;
require Exporter;
use strict;

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);

@ISA    = qw(Exporter);
@EXPORT = qw(
    NetGroup_Exists
    NetGroup_ExistsPrefix
    NetGroup_ExistsMulti
    NetGroup_List
    NetGroup_List_Changed
    NetGroup_Create
    NetGroup_Delete
    NetGroup_AddMemberUser
    NetGroup_AddMemberUsers
    NetGroup_DeleteMemberUser
    NetGroup_DeleteMemberUsers
    NetGroup_MemberUsers
    NetGroup_MemberUsersMulti
    NetGroup_MemberUserToGroups
    NetGroup_MemberOf
    NetGroup_MemberOfMulti
    NetGroup_GetDescriptionMulti
    NetGroup_SetDescription
    NetGroup_GetNotesMulti
    NetGroup_SetNotes
);

use Local::UsageLogger;
use Local::SimpleRPC;

use MST::Env;

our $ADMIN_RPC;

# Begin-Doc
# Name: _NIS_syslog
# Type: function
# Access: internal
# Description: wrapper around syslog function to allow it to be ignored on windows
# End-Doc
sub _NIS_syslog {
    my @args = @_;

    # Allow code to function on windows
    eval "use Sys::Syslog";
    eval { syslog(@args); };
}

# Begin-Doc
# Name: NetGroup_SimpleRPC
# Type: function
# Description: Helper routine to set up RPC connection for user apps
# Syntax: $rpc = &NetGroup_SimpleRPC()
# Access: internal
# End-Doc
sub NetGroup_SimpleRPC {

    &LogAPIUsage();

    if ($ADMIN_RPC) {
        return $ADMIN_RPC;
    }

    my $rpchost;
    my $env = &MST_Env();

    if ( $env eq "dev" ) {
        $rpchost = "itrpc-groups-dev.mst.edu";
    }
    elsif ( $env eq "test" ) {
        $rpchost = "itrpc-groups-test.mst.edu";
    }
    else {
        $rpchost = "itrpc-groups.mst.edu";
    }

    $ADMIN_RPC = new Local::SimpleRPC::Client(
        base_url => "https://${rpchost}/auth-perl-bin/UserGroup",
        retries  => 2
    );

    return $ADMIN_RPC;
}

# Begin-Doc
# Name: NetGroup_Exists
# Type: function
# Description: returns true if user netgroup exists
# Syntax: $res = &NetGroup_Exists($group)
# End-Doc
sub NetGroup_Exists {
    my $group = shift;

    &LogAPIUsage();

    my $rpc = &NetGroup_SimpleRPC();
    my $info = $rpc->Exists( group => $group );

    return int( $info->{$group} );
}

# Begin-Doc
# Name: NetGroup_ExistsPrefix
# Type: function
# Description: returns true if user netgroup or any prefix of that user netgroup exist
# Syntax: $res = &NetGroup_ExistsPrefix($group)
# End-Doc
sub NetGroup_ExistsPrefix {
    my $group = shift;

    &LogAPIUsage();

    my @tmp = split( '-', $group );
    my @groups = ();
    for ( my $i = 0; $i <= $#tmp; $i++ ) {
        my $tmpgrp = join( "-", @tmp[ 0 .. $i ] );
        push( @groups, $tmpgrp );
    }

    my $res = &NetGroup_ExistsMulti(@groups);
    if ( !$res ) {
        return 0;
    }

    foreach my $grp (@groups) {
        if ( $res->{$grp} ) {
            return 1;
        }
    }
    return 0;
}

# Begin-Doc
# Name: NetGroup_ExistsMulti
# Type: function
# Description: returns true if user netgroups exist
# Syntax: $res = &NetGroup_ExistsMulti($group, $group, ...)
# Returns: hash keyed on group, value is 1 if group exists
# End-Doc
sub NetGroup_ExistsMulti {
    my @groups = @_;
    my @args;

    &LogAPIUsage();

    foreach my $grp (@groups) {
        push( @args, "group", $grp );
    }

    my $rpc  = &NetGroup_SimpleRPC();
    my $info = $rpc->Exists(@args);

    return $info;
}

# Begin-Doc
# Name: NetGroup_List
# Type: function
# Description: Returns array of user netgroups
# Syntax: @groups = &NetGroup_List
# End-Doc
sub NetGroup_List {
    &LogAPIUsage();

    my $rpc  = &NetGroup_SimpleRPC();
    my $info = $rpc->List();

    return @$info;
}

# Begin-Doc
# Name: NetGroup_List_Changed
# Type: function
# Description: Returns array of recently changed user netgroups
# Syntax: @groups = &NetGroup_List_Changed
# End-Doc
sub NetGroup_List_Changed {
    &LogAPIUsage();

    my $rpc  = &NetGroup_SimpleRPC();
    my $info = $rpc->ListChanged();

    return @$info;
}

# Begin-Doc
# Name: NetGroup_Create
# Type: function
# Description: Creates a user netgroup
# Syntax: &NetGroup_Create($group)
# Returns: zero or undef on success, error msg on failure
# End-Doc
sub NetGroup_Create {
    my $group = shift;

    &LogAPIUsage();

    my $rh = $ENV{HTTP_X_FORWARDED_FOR} || $ENV{REMOTE_HOST} || $ENV{REMOTE_ADDR};
    &_NIS_syslog( "info",
              "NetGroup_Create($group) by "
            . $ENV{REMOTE_USER}
            . " from host "
            . $rh );

    my $rpc = &NetGroup_SimpleRPC();
    eval { $rpc->Create( group => $group ); };

    return $@;
}

# Begin-Doc
# Name: NetGroup_Delete
# Type: function
# Description: Deletes a user netgroup and all members
# Syntax: &NetGroup_Delete($group)
# Returns: zero or undef on success, error msg on failure
# End-Doc
sub NetGroup_Delete {
    my ($group) = @_;

    &LogAPIUsage();

    my $rh = $ENV{HTTP_X_FORWARDED_FOR} || $ENV{REMOTE_HOST} || $ENV{REMOTE_ADDR};
    &_NIS_syslog( "info",
              "NetGroup_Delete($group) by "
            . $ENV{REMOTE_USER}
            . " from host "
            . $rh );

    my $rpc = &NetGroup_SimpleRPC();
    eval { $rpc->Delete( group => $group ); };

    return $@;
}

# Begin-Doc
# Name: NetGroup_AddMemberUser
# Type: function
# Description: Adds a user to a user netgroup
# Syntax: &NetGroup_AddMemberUser($user, $group)
# Comments: This routine is deprecated, use AddMemberUsers instead.
# End-Doc
sub NetGroup_AddMemberUser {
    my ( $user, $group ) = @_;
    &LogAPIUsage();

    return &NetGroup_AddMemberUsers( $group, $user );
}

# Begin-Doc
# Name: NetGroup_AddMemberUsers
# Type: function
# Description: Adds a user to a user netgroup
# Syntax: &NetGroup_AddMemberUsers($group, @users)
# Comments: Note the argument ordering.
# End-Doc
sub NetGroup_AddMemberUsers {
    my ( $group, @users ) = @_;

    &LogAPIUsage();

    my $rh = $ENV{HTTP_X_FORWARDED_FOR} || $ENV{REMOTE_HOST} || $ENV{REMOTE_ADDR};
    &_NIS_syslog( "info",
              "NetGroup_AddMemberUsers("
            . join( ",", @users )
            . " to $group) by "
            . $ENV{REMOTE_USER}
            . " from host "
            . $rh );

    @users = map {lc} @users;
    $group = lc $group;

    my @args;
    push( @args, "group", $group );
    foreach my $user (@users) {
        push( @args, "user", $user );
    }

    my $rpc = &NetGroup_SimpleRPC();
    eval { $rpc->AddMemberUsers(@args); };
    return $@;
}

# Begin-Doc
# Name: NetGroup_DeleteMemberUser
# Type: function
# Description: Deletes a user from a user netgroup
# Syntax: &NetGroup_DeleteMemberUser($host, $group)
# Comments: This routine is deprecated, use DeleteMemberUsers instead.
# End-Doc
sub NetGroup_DeleteMemberUser {
    my ( $user, $group ) = @_;
    &LogAPIUsage();

    return &NetGroup_DeleteMemberUsers( $group, $user );
}

# Begin-Doc
# Name: NetGroup_DeleteMemberUsers
# Type: function
# Description: Deletes one or more user from a user netgroup
# Syntax: &NetGroup_DeleteMemberUsers($group, @users)
# Comments: Note the argument ordering.
# End-Doc
sub NetGroup_DeleteMemberUsers {
    my ( $group, @users ) = @_;

    &LogAPIUsage();

    my $rh = $ENV{HTTP_X_FORWARDED_FOR} || $ENV{REMOTE_HOST} || $ENV{REMOTE_ADDR};
    &_NIS_syslog( "info",
              "NetGroup_DeleteMemberUsers("
            . join( ",", @users )
            . " from $group) by "
            . $ENV{REMOTE_USER}
            . " from host "
            . $rh );

    @users = map {lc} @users;
    $group = lc $group;

    my @args;
    push( @args, "group", $group );
    foreach my $user (@users) {
        push( @args, "user", $user );
    }

    my $rpc = &NetGroup_SimpleRPC();
    eval { $rpc->DeleteMemberUsers(@args); };
    return $@;
}

# Begin-Doc
# Name: NetGroup_MemberUsers
# Type: function
# Description: Returns array of users that are members of $group
# Syntax: @hosts = &NetGroup_MemberUsers($group)
# End-Doc
sub NetGroup_MemberUsers {
    my $group = shift;

    &LogAPIUsage();

    my $rpc = &NetGroup_SimpleRPC();
    my $info = $rpc->MemberUsers( group => $group );
    return @{ $info->{$group} };
}

# Begin-Doc
# Name: NetGroup_MemberUsersMulti
# Type: function
# Description: Returns list of users that are members of each group
# Syntax: %members_by_group = &NetGroup_MemberUsersMulti($group, $group, ...)
# End-Doc
sub NetGroup_MemberUsersMulti {
    my @groups = @_;
    my @args;

    &LogAPIUsage();

    foreach my $grp (@groups) {
        push( @args, "group", $grp );
    }

    my $rpc  = &NetGroup_SimpleRPC();
    my $info = $rpc->MemberUsers(@args);
    return %$info;
}

# Begin-Doc
# Name: NetGroup_MemberUserToGroups
# Type: function
# Description: Returns array of groups that user $user is a member of
# Syntax: @groups = &NetGroup_MemberUserToGroups($user)
# Comments: DEPRECATED - use MemberOf instead
# End-Doc
sub NetGroup_MemberUserToGroups {
    &LogAPIUsage();

    return &NetGroup_MemberOf(@_);
}

# Begin-Doc
# Name: NetGroup_MemberOf
# Type: function
# Description: Returns array of groups that user $user is a member of
# Syntax: @groups = &NetGroup_MemberOf($user)
# End-Doc
sub NetGroup_MemberOf {
    my $user = shift;

    &LogAPIUsage();

    my $rpc = &NetGroup_SimpleRPC();
    my $info = $rpc->MemberOf( user => $user );
    return @{ $info->{$user} };
}

# Begin-Doc
# Name: NetGroup_MemberOfMulti
# Type: function
# Description: Returns array of groups that each user is a member of
# Syntax: %groups_by_user = &NetGroup_MemberOfMulti($user, $user, ...)
# Returns: hash keyed on user, values are arrays of groups
# End-Doc
sub NetGroup_MemberOfMulti {
    my @users = @_;
    my @args;

    &LogAPIUsage();

    foreach my $user (@users) {
        push( @args, "user", $user );
    }

    my $rpc  = &NetGroup_SimpleRPC();
    my $info = $rpc->MemberOf(@args);
    return %$info;
}

# Begin-Doc
# Name: NetGroup_GetDescriptionMulti
# Type: function
# Description: Returns list of description fields for groups
# Syntax: %desc_by_group = &NetGroup_GetDescriptionMulti($group, $group, ...)
# End-Doc
sub NetGroup_GetDescriptionMulti {
    my @groups = @_;
    my @args;

    &LogAPIUsage();

    foreach my $grp (@groups) {
        push( @args, "group", $grp );
    }

    my $rpc  = &NetGroup_SimpleRPC();
    my $info = $rpc->GetDescription(@args);
    return %$info;
}

# Begin-Doc
# Name: NetGroup_SetDescription
# Type: function
# Description: Sets a description for a netgroup
# Syntax: $res = &NetGroup_SetDescription($group, $desc);
# Comments: returns error msg on failure
# End-Doc
sub NetGroup_SetDescription {
    my $group = shift;
    my $desc  = shift;

    &LogAPIUsage();

    my $rpc = &NetGroup_SimpleRPC();
    eval { $rpc->SetDescription( group => $group, description => $desc ) };
    return $@;
}

# Begin-Doc
# Name: NetGroup_GetNotesMulti
# Type: function
# Description: Returns list of notes fields for groups
# Syntax: %desc_by_group = &NetGroup_GetNotesMulti($group, $group, ...)
# End-Doc
sub NetGroup_GetNotesMulti {
    my @groups = @_;
    my @args;

    &LogAPIUsage();

    foreach my $grp (@groups) {
        push( @args, "group", $grp );
    }

    my $rpc  = &NetGroup_SimpleRPC();
    my $info = $rpc->GetNotes(@args);
    return %$info;
}

# Begin-Doc
# Name: NetGroup_SetNotes
# Type: function
# Description: Sets notes for a netgroup
# Syntax: $res = &NetGroup_SetNotes($group, $notes);
# Comments: returns error msg on failure
# End-Doc
sub NetGroup_SetNotes {
    my $group = shift;
    my $notes = shift;

    &LogAPIUsage();

    my $rpc = &NetGroup_SimpleRPC();
    eval { $rpc->SetNotes( group => $group, notes => $notes ) };
    return $@;
}

1;

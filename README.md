Planned transitions from umrperl to mstperl:

  * UMR::SysProg::NIS::UserGroup -> MST::NetGroup
     * NIS_UserGroup_* -> NetGroup_*
  * UMR::SysProg::NIS::Access -> MST::NetGroupAccess
     * NIS_Access_* -> NetGroupAccess_*

  * UMR::SysProg::SetUID -> Local::SetUID (from perllib)
  * UMR::UsageLogger -> MST::UsageLogger (overrides stub in perllib)
  * UMR::PrivSys -> MST::PrivSys (overrides stub in perllib)

  * UMR::SysProg::ADSObject -> Local::ADSObject (from perllib)
  * UMR::SysProg::UMSSO -> MST::UM::SSO
  * UMR::GoogleAdmin -> discontinued
  * UMR::Exchange -> discontinued
  * UMR::Env -> MST::Env
     * UMR_Env() -> MST_Env()

  * UMR::CurrentUser -> Local::CurrentUser
     * UMR_CurrentUser() -> Local_CurrentUser()
  * UMR::Logger -> discontinued
  * UMR::* -> Local::*
